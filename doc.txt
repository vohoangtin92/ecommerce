*/
-b1: cai laravel composer, Livewire, laravel/ui

-b2: Make login & registration system & integrate bootstrap 5 & Livewire Setup
    import css, js from bundle
    migrate table

-b3: How to setup Admin Panel for ecommerce in laravel 9
-b4: How to make Admin Middleware in laravel | redirect admin login to dashboard - abandon user login into other admin dashboard
-Part 5: How to make admin logout system in laravel 9 for ecommerce - integrate logout admin dashboard into admin Panel
-Part 6: Add Category in ecommerce with creating migration, model, controller - migrate sau khi tao table Category, model Category, 
    ontroller Category, create route to web.
    create view index.php
    Hàm str trả về một thể hiện mới Illuminate\Support\Stringable chuỗi đã cho. Hàm này tương đương với phương thức Str::of:
    slug: custom lại URL từ một chuỗi nhất định, thì bạn có thể dùng Str::slug:
    $slug = Str::slug('Laravel Is Awesome'); -> mac dinh dau cach la (-)
    $slug: "laravel-is-awesome" 
    $slug = Str::slug('Laravel Is Awesome','&');

-Part 7: Category View, edit update, confirm delete using Livewire
    create Livewire component
    add wire component view update
    create controller update
    create function delete in index.php
    import script to close modal



/*